#pragma once

#include <vector>

#include <QString>
#include <QPixmap>

namespace Data
{

enum class ProjStatus
{
    Active,
    Inactive
};

struct Project
{
    int id;

    QString name;
    QImage img;

    ProjStatus status;

    QString timeWeek;
    QString timeMonth;
    QString timeTotal;

    Project():
        id(-1),
        status(ProjStatus::Inactive)
    {}
};

using ProjList = std::vector<Project>;

}

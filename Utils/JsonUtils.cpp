#include "JsonUtils.hpp"

#include <QJsonDocument>

QString Utils::ObjToStr(const QJsonObject& obj)
{
    QJsonDocument doc;
    doc.setObject(obj);

    return QString::fromUtf8(doc.toJson());
}

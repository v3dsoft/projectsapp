#include "TitleLbl.hpp"

namespace
{

const QString lblStyle = R"style(
    QLabel {
        font-size: 14pt;
        font-weight: 600;
        text-transform: uppercase;
    }
)style";

}

UI::TitleLbl::TitleLbl(const QString& text, QWidget *parent):
        QLabel(text, parent)
{
    setStyleSheet(lblStyle);
}

#pragma once

#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include "Controls/TitleLbl.hpp"
#include "Controls/SimpleInput.hpp"
#include "Controls/SimpleBtn.hpp"

namespace UI
{

class LoginDlg:
    public QDialog
{
    Q_OBJECT

public:
    LoginDlg(QWidget *parent = nullptr);
    ~LoginDlg();

    QString GetEmail() const;
    QString GetPassword() const;

private:
    void InitUI();

    bool DataValid() const;

private slots:
    void OnLoginClicked();

private:
    QVBoxLayout* mainBox;

    TitleLbl* titleLbl;
    SimpleInput* mailEdt;
    SimpleInput* passwordEdt;
    SimpleBtn* loginBtn;
};

}

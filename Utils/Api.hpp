#pragma once

#include <QString>
#include <QJsonObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QImage>

#include <vector>

class QNetworkReply;

namespace Utils
{

struct PropsItem {
    QString name;
    QString value;

    inline PropsItem(QString n, QString v):
        name(std::move(n)),
        value(std::move(v))
    {}
};

using PropsMap = std::vector<PropsItem>;

class Api:
    public QObject
{
    Q_OBJECT

public:
    explicit Api(QString base);

    QJsonObject Get(const QString& endpoint, const PropsMap& props, const QString& token = "");
    QJsonObject Post(const QString& endpoint, const QJsonObject& data, const QString& token = "");

    QImage LoadImage(const QString& imgUrl);

private:
    QUrl PrepareGetUrl(const QString& endpoint, const PropsMap& props)const;

    static void WaitForReply(QNetworkReply* reply);

private:
    QString baseUrl;

    QNetworkAccessManager* network;
};

}

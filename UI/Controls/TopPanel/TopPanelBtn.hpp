#pragma once

#include <QPushButton>

namespace UI
{

class TopPanelBtn:
    public QPushButton
{
Q_OBJECT

public:
    TopPanelBtn(const QString& text, QWidget *parent = nullptr);
};

}

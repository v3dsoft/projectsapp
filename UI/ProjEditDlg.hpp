#pragma once

#include <QDialog>
#include <QHBoxLayout>
#include <QLabel>

#include "Controls/SimpleInput.hpp"
#include "Controls/SimpleBtn.hpp"

#include "../Data/Project.hpp"

namespace UI
{

class ProjEditDlg:
    public QDialog
{
    Q_OBJECT

public:
    explicit ProjEditDlg(const Data::Project& initData, QWidget *parent = nullptr);

    QString GetProjectName()const;

private:
    void InitUI(const Data::Project& initData);

protected slots:
    void OnOk();

private:
    QHBoxLayout* mainBox;

    QLabel* nameLbl;
    SimpleInput* nameEdt;

    SimpleBtn* okBtn;
};

}

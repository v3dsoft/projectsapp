#pragma once

#include <QAbstractListModel>

#include "../../../Data/Project.hpp"

namespace UI
{

class ProjListModel:
    public QAbstractListModel
{
    Q_OBJECT

public:
    ProjListModel(const Data::ProjList& pl, QObject* parent = nullptr);

    const Data::Project& GetItem(int row)const;

public: // QAbstractListModel overrides
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

private:
    const Data::ProjList& projList;
};

}

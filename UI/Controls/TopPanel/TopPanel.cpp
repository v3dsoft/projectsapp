#include "TopPanel.hpp"

#include <QStyleOption>
#include <QPainter>

namespace
{

const QString tpStyle = R"style(
    QFrame#TopPanel {
        border-bottom: 2px solid #878787;
        background: #fafafa;
    }
)style";

}

UI::TopPanel::TopPanel(QWidget *parent):
    QFrame(parent),

    mainBox(nullptr),
    mainIcon(nullptr),
    logoutBtn(nullptr)
{
    InitUI();
}

void UI::TopPanel::InitUI()
{
    setObjectName("TopPanel");

    setFrameStyle(QFrame::StyledPanel);
    setStyleSheet(tpStyle);

    mainBox = new QHBoxLayout();
    mainBox->setContentsMargins(24, 8, 24, 8);
    setLayout(mainBox);

    mainIcon = new QLabel();
    mainIcon->setPixmap(QPixmap(":/QLogo.png"));
    mainBox->addWidget(mainIcon);

    mainBox->addStretch();

    logoutBtn = new TopPanelBtn("Logout");
    mainBox->addWidget(logoutBtn);
    connect(logoutBtn, SIGNAL(clicked()), this, SIGNAL(logout()));
}

void UI::TopPanel::paintEvent(QPaintEvent *) {
    QStyleOption opt;
    opt.init(this);

    QPainter p(this);

    style()->drawPrimitive(QStyle::PE_Widget, &opt, &p, this);
}

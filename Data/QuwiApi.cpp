#include "QuwiApi.hpp"

#include <QJsonArray>

const QString Data::QuwiAPI::apiBase = "https://api.quwi.com/v2";

QString CreateTimestamp(int total)
{
    int seconds = total % 60;
    total /= 60;

    int minutes = total % 60;
    total /= 60;

    return QString("%1:%2:%3").arg(total).arg(minutes).arg(seconds);
}

Data::QuwiAPI::QuwiAPI(const QString &email, const QString &password):
    api(apiBase)
{
    Login(email, password);
}

Data::ProjList Data::QuwiAPI::GetProjectList()
{
    ProjList projList;

    auto res = api.Get("projects-manage/index", {}, token);

    auto projListData = res.find("projects");
    if (projListData->isArray())
    {
        auto projListArr = projListData->toArray();
        for (const auto& projDataRef: projListArr)
        {
            if (!projDataRef.isObject())
                continue;

            auto projData = projDataRef.toObject();

            Project newProj;

            auto idData = projData.find("id");
            if (!idData->isDouble())
                continue;
            newProj.id = static_cast<int>(idData->toDouble());

            auto nameData = projData.find("name");
            if (!nameData->isString())
                continue;
            newProj.name = nameData->toString();

            auto imgData = projData.find("logo_url");
            if (imgData->isString())
            {
                auto imgUrl = imgData->toString();
                newProj.img = api.LoadImage(imgUrl);
            }

            auto activeData = projData.find("is_active");
            if (activeData->isDouble())
            {
                bool isActive = (static_cast<int>(activeData->toDouble()) == 1);
                newProj.status = isActive ? ProjStatus::Active : ProjStatus::Inactive;
            }

            auto timeWeekData = projData.find("spent_time_week");
            if (timeWeekData->isDouble())
            {
                int timeWeek = static_cast<int>(timeWeekData->toDouble());
                newProj.timeWeek = CreateTimestamp(timeWeek);
            }

            auto timeMonthData = projData.find("spent_time_month");
            if (timeMonthData->isDouble())
            {
                int timeMonth = static_cast<int>(timeMonthData->toDouble());
                newProj.timeMonth = CreateTimestamp(timeMonth);
            }

            auto timeTotalData = projData.find("spent_time_all");
            if (timeTotalData->isDouble())
            {
                int timeTotal = static_cast<int>(timeTotalData->toDouble());
                newProj.timeTotal = CreateTimestamp(timeTotal);
            }

            projList.push_back(newProj);
        }
    }

    return projList;
}

void Data::QuwiAPI::SetProjectName(int projectId, const QString &newName)
{
    auto endpoint = QString("projects-manage/update?id=%1").arg(projectId);

    QJsonObject data
    {
            {"name", newName}
    };

    auto res = api.Post(endpoint, data, token);

    auto nameData = res.find("project");
    if (!nameData->isObject())
        throw std::runtime_error("Unable to change project name");
}

void Data::QuwiAPI::Login(const QString &email, const QString &password)
{
    QJsonObject data
    {
        {"email", email},
        {"password", password}
    };

    auto res = api.Post("auth/login", data);
    auto tokenData = res.find("token");

    if (tokenData->isString())
        token = tokenData->toString();
    else
        throw std::runtime_error("Login failed");
}

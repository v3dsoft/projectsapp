#pragma once

#include <QString>

#include "../Utils/Api.hpp"
#include "Project.hpp"

namespace Data
{

class QuwiAPI
{
private:
    static const QString apiBase;

public:
    QuwiAPI(const QString& email, const QString& password);

    ProjList GetProjectList();

    void SetProjectName(int projectId, const QString& newName);

private:
    void Login(const QString& email, const QString& password);

private:
    Utils::Api api;
    QString token;
};

}

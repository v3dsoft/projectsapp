#include "LoginDlg.hpp"

#include <QValidator>
#include <QMessageBox>

UI::LoginDlg::LoginDlg(QWidget *parent):
    QDialog(parent),

    mainBox(nullptr),

    titleLbl(nullptr),
    mailEdt(nullptr),
    passwordEdt(nullptr),
    loginBtn(nullptr)
{
    InitUI();
}

UI::LoginDlg::~LoginDlg()
{
}

QString UI::LoginDlg::GetEmail() const
{
    return mailEdt->text();
}

QString UI::LoginDlg::GetPassword() const
{
    return passwordEdt->text();
}

void UI::LoginDlg::InitUI()
{
    setWindowTitle("Login");
    setContentsMargins(25, 10, 25, 15);

    mainBox = new QVBoxLayout();
    mainBox->setSizeConstraint( QLayout::SetFixedSize );
    setLayout(mainBox);

    titleLbl = new TitleLbl("Login");
    mainBox->addWidget(titleLbl);

    mainBox->addSpacing(12);

    QRegExp mailRegExp(R"reg((?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\]))reg");
    QValidator* mailValidator = new QRegExpValidator(mailRegExp);

    mailEdt = new SimpleInput();
    mailEdt->setPlaceholderText("Email");
    mailEdt->setValidator(mailValidator);
    mainBox->addWidget(mailEdt);

    passwordEdt = new SimpleInput();
    passwordEdt->setPlaceholderText("Password");
    passwordEdt->setEchoMode(QLineEdit::Password);
    mainBox->addWidget(passwordEdt);

    mainBox->addSpacing(16);

    loginBtn = new SimpleBtn("Login");
    loginBtn->setMaximumWidth(106);
    mainBox->addWidget(loginBtn);
    connect(loginBtn, SIGNAL(clicked()), this, SLOT(OnLoginClicked()));

    //Test credentials
    //mailEdt->setText("vitaliibondtest@gmail.com");
    //passwordEdt->setText("vitaliibondtest");
}

bool UI::LoginDlg::DataValid() const
{
    if (!mailEdt->hasAcceptableInput() || mailEdt->text().isEmpty())
        return false;

    if (passwordEdt->text().isEmpty())
        return false;

    return true;
}

void UI::LoginDlg::OnLoginClicked()
{
    if (DataValid())
        accept();
    else
    {
        QMessageBox::critical(this, "Login", "Please check the data you entered");
    }
}

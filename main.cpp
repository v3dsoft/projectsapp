#include <memory>

#include <QApplication>
#include <QMessageBox>

#include "Data/QuwiApi.hpp"

#include "UI/MainWindow.hpp"
#include "UI/LoginDlg.hpp"

#include <QSslSocket>

int main(int argc, char *argv[])
{
    auto sslLib = QSslSocket::sslLibraryBuildVersionString().toStdString();

    #ifdef WIN32
        QApplication::addLibraryPath("./plugins");
    #endif

    QApplication a(argc, argv);

    std::unique_ptr<Data::QuwiAPI> api;

    UI::LoginDlg loginDlg;
    while (!api) {
        if (loginDlg.exec() == QDialog::Accepted)
        {
            try
            {
                api = std::make_unique<Data::QuwiAPI>(loginDlg.GetEmail(), loginDlg.GetPassword());
            }
            catch (...)
            {
                QMessageBox::critical(nullptr, "Login", "Login failed. Please check your credentials.");
            }
        }
        else
            return EXIT_SUCCESS;
    }

    auto projList = api->GetProjectList();

    UI::MainWindow w(projList, *api);
    w.show();

    return a.exec();
}

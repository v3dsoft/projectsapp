#include "SimpleBtn.hpp"

namespace
{

const QString btnStyle = R"style(
    QPushButton {
        border: none;
        border-radius: 3px;
        background: #395378;

        padding: 8px;

        font-weight: bold;
        color: white;
        text-transform: uppercase;
    }

    QPushButton:hover {
        background: #2d415e;
    }

    QPushButton:pressed {
        background: #1b283a;
    }
)style";

}

UI::SimpleBtn::SimpleBtn(const QString& text, QWidget *parent):
    QPushButton(text, parent)
{
    setStyleSheet(btnStyle);
}

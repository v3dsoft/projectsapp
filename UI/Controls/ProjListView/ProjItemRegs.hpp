#pragma once

#include <QRect>

namespace UI
{

class ProjItemRegs
{
public:
    static const int statusWidth = 100;
    static const int timeWidth = 180;

    QRect imgRect;
    QRect nameRect;
    QRect statusRect;
    QRect timesRect;

public:
    ProjItemRegs(const QRect& bodyRect, int padding)
    {
        int vertSize = bodyRect.height() - padding * 2;
        imgRect = QRect(
            bodyRect.x() + padding,
            bodyRect.y() + padding,
            vertSize,
            vertSize
        );

        int currX = bodyRect.width() - padding;

        currX -= timeWidth;
        timesRect = QRect(
            bodyRect.x() + currX,
            bodyRect.y() + padding,
            timeWidth,
            vertSize
        );

        currX -= padding;

        currX -= statusWidth;
        statusRect = QRect(
            bodyRect.x() + currX,
            bodyRect.y() + padding,
            statusWidth,
            vertSize
        );

        currX -= padding;

        int nameX = padding + vertSize + padding;
        int nameWidth = currX - nameX;
        nameRect = QRect(
            bodyRect.x() + nameX,
            bodyRect.y() + padding,
            nameWidth,
            vertSize
        );
    }
};

}

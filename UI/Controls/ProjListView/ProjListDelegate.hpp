#pragma once

#include <QStyledItemDelegate>

#include "../../../Data/Project.hpp"

namespace UI
{

class ProjListDelegate:
    public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit ProjListDelegate(QObject *parent = nullptr);

public: // QStyledItemDelegate overrides
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const override;

private:
    static QRect CalcItemBodyRect(const QRect& rowRect);
    static void DrawImageInRect(QPainter *painter, const QRect& imgRect, const QImage& img);
    static void DrawName(QPainter *painter, const QRect& nameRect, const Data::Project& item);
    static void DrawStatus(QPainter *painter, const QRect& statusRect, const Data::Project& item);
    static void DrawTimes(QPainter *painter, const QRect& timesRect, const Data::Project& item);
};

}

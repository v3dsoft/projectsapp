#include "ProjListModel.hpp"

UI::ProjListModel::ProjListModel(const Data::ProjList &pl, QObject* parent):
    QAbstractListModel(parent),

    projList(pl)
{
}

const Data::Project& UI::ProjListModel::GetItem(int row) const
{
    return projList[row];
}

int UI::ProjListModel::rowCount(const QModelIndex &parent) const
{
    return projList.size();
}

QVariant UI::ProjListModel::data(const QModelIndex &index, int role) const
{
    int row = index.row();
    if ((role == Qt::DisplayRole) && (row < projList.size()))
    {
        return QVariant(projList[row].name);
    }

    return QVariant();
}

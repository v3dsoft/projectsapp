#pragma once

#include <QPushButton>

namespace UI
{

class SimpleBtn:
    public QPushButton
{
    Q_OBJECT

public:
    SimpleBtn(const QString& text, QWidget *parent = nullptr);
};

}

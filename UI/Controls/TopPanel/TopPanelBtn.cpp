#include "TopPanelBtn.hpp"

namespace
{

const QString btnStyle = R"style(
    QPushButton {
        border: none;
        background: none;

        padding: 8px 4px;

        font-weight: bold;
        color: #66686d;
        text-transform: uppercase;
    }

    QPushButton:hover {
        color: #505155;
    }

    QPushButton:pressed {
        color: #36373a;
    }
)style";

}

UI::TopPanelBtn::TopPanelBtn(const QString& text, QWidget *parent):
    QPushButton(text, parent)
{
    setStyleSheet(btnStyle);
}

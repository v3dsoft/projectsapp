#pragma once

#include <QLabel>

namespace UI
{

class TitleLbl:
    public QLabel
{
Q_OBJECT

public:
    TitleLbl(const QString& text, QWidget *parent = nullptr);
};

}

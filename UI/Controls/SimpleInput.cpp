#include "SimpleInput.hpp"

namespace
{

const QString inputStyle = R"style(
    QLineEdit {
        padding: 8px;
        border: 1px solid #cecece;
        border-radius: 2px;
        font-size: 10pt;
        min-width: 300px;
    }
)style";

}

UI::SimpleInput::SimpleInput(QWidget *parent):
    QLineEdit(parent)
{
    setStyleSheet(inputStyle);
}

#include "ProjListDelegate.hpp"

#include <QtGui/QPainter>
#include <QPainterPath>

#include "ProjItemRegs.hpp"
#include "ProjListModel.hpp"

#include "../../../Utils/SizeUtils.hpp"

namespace
{

const int itemBodyMargin = 4;
const int itemBodyMaxWidth = 800;
const int itemBodyMaxWidthMargins = itemBodyMaxWidth + itemBodyMargin * 2;

}

UI::ProjListDelegate::ProjListDelegate(QObject *parent):
    QStyledItemDelegate(parent)
{
}

QSize UI::ProjListDelegate::sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    return QSize(600, 140);
}

void UI::ProjListDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    const auto* mdl = dynamic_cast<const ProjListModel*>(index.model());
    const auto& item = mdl->GetItem(index.row());

    painter->setRenderHint(QPainter::Antialiasing);

    QTransform ppTr;
    ppTr.translate(0.5, 0.5);   // For pixel-perfect lines
    ppTr.translate(0.0, option.rect.y());
    painter->setTransform(ppTr);

    auto bodyRect = CalcItemBodyRect(option.rect);
    QPainterPath bodyPath;
    bodyPath.addRoundedRect(bodyRect, 2, 2);

    QPen bodyPen(QColor(222,222, 222), 1);
    painter->setPen(bodyPen);

    painter->fillPath(bodyPath, Qt::white);
    painter->drawPath(bodyPath);

    const ProjItemRegs itemRegs(bodyRect, 16);

    DrawImageInRect(painter, itemRegs.imgRect, item.img);
    DrawName(painter, itemRegs.nameRect, item);
    DrawStatus(painter, itemRegs.statusRect, item);
    DrawTimes(painter, itemRegs.timesRect, item);
}

QRect UI::ProjListDelegate::CalcItemBodyRect(const QRect& rowRect)
{
    if(rowRect.width() <= itemBodyMaxWidthMargins)
    {
        return QRect(
            itemBodyMargin,
            itemBodyMargin,
            rowRect.width() - itemBodyMargin * 2,
            rowRect.height() - itemBodyMargin * 2
        );
    }
    else
    {
        return QRect(
            (rowRect.width() - itemBodyMaxWidth) / 2,
            itemBodyMargin,
            itemBodyMaxWidth,
            rowRect.height() - itemBodyMargin * 2
        );
    }
}

void UI::ProjListDelegate::DrawImageInRect(QPainter *painter, const QRect& imgRect, const QImage& img)
{
    auto fitImgRect = Utils::FitImgRect(imgRect, img.rect().size());
    painter->drawImage(fitImgRect, img);
}

void UI::ProjListDelegate::DrawName(QPainter *painter, const QRect& nameRect, const Data::Project& item)
{
    QFont nameFont("Arial", 12);

    painter->setPen(Qt::black);
    painter->setFont(nameFont);
    painter->drawText(nameRect, Qt::AlignLeft | Qt::AlignVCenter, item.name);
}

void UI::ProjListDelegate::DrawStatus(QPainter *painter, const QRect& statusRect, const Data::Project& item)
{
    QFont statusFont("Arial", 12);

    QString text = (item.status == Data::ProjStatus::Active) ? "Active" : "Inactive";
    QColor color = (item.status == Data::ProjStatus::Active) ? QColor(0, 128, 0) : Qt::darkRed;

    painter->setPen(color);
    painter->setFont(statusFont);
    painter->drawText(statusRect, Qt::AlignLeft | Qt::AlignVCenter, text);
}

void UI::ProjListDelegate::DrawTimes(QPainter *painter, const QRect& timesRect, const Data::Project& item)
{
    auto text = QString("This week:\t%1\n\nThis month:\t%2\n\nTotal:\t%3")
            .arg(item.timeWeek)
            .arg(item.timeMonth)
            .arg(item.timeTotal);

    QFont timeFont("Arial", 12);

    painter->setPen(Qt::black);
    painter->setFont(timeFont);
    painter->drawText(timesRect, Qt::AlignLeft | Qt::AlignVCenter, text);
}

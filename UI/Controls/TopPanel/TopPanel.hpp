#pragma once

#include <QFrame>
#include <QHBoxLayout>
#include <QLabel>

#include "TopPanelBtn.hpp"

namespace UI
{

class TopPanel:
    public QFrame
{
    Q_OBJECT

public:
    TopPanel(QWidget *parent = nullptr);

private:
    void InitUI();

signals:
    void logout();

protected:
    void paintEvent(QPaintEvent *) override;

private:
    QHBoxLayout* mainBox;
    QLabel* mainIcon;
    TopPanelBtn* logoutBtn;
};

}

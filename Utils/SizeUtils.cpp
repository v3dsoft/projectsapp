#include "SizeUtils.hpp"

QRect Utils::FitImgRect(const QRect& boundRect, const QSize& imgSize)
{
    auto boundWidth = boundRect.width();
    auto boundHeight = boundRect.height();

    auto imgWidth = imgSize.width();
    auto imgHeight = imgSize.height();

    float imgAspect = (float)imgWidth / (float)imgHeight;

    if (imgWidth > boundWidth)
    {
        imgWidth = boundWidth;
        imgHeight = (int)((float)imgWidth / imgAspect);
    }

    if (imgHeight > boundHeight)
    {
        imgHeight = boundHeight;
        imgWidth = (int)((float)imgHeight * imgAspect);
    }

    return QRect(
        boundRect.x() + (boundWidth - imgWidth) / 2,
        boundRect.y() + (boundHeight - imgHeight) / 2,
        imgWidth,
        imgHeight
    );
}

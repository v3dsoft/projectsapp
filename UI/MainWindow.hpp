#pragma once

#include <QVBoxLayout>

#include "Controls/TopPanel/TopPanel.hpp"
#include "Controls/ProjListView/ProjListView.hpp"

#include "../Data/Project.hpp"
#include "../Data/QuwiApi.hpp"

namespace UI
{

class MainWindow:
    public QWidget
{
    Q_OBJECT

public:
    MainWindow(Data::ProjList& pl, Data::QuwiAPI& api, QWidget *parent = nullptr);
    ~MainWindow();

private:
    void InitUI();

protected slots:
    void OnLogout();
    void OnEditProject(int index);

private:
    QVBoxLayout* mainBox;

    TopPanel* topPanel;
    ProjListView* projListView;

    Data::ProjList& projList;
    Data::QuwiAPI& quwiApi;
};

}

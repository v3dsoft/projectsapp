#include "Api.hpp"

#include <stdexcept>
#include <sstream>

#include <QJsonDocument>
#include <QEventLoop>
#include <QNetworkReply>

Utils::Api::Api(QString base):
    baseUrl(std::move(base)),

    network(nullptr)
{
    if (baseUrl.isEmpty())
        throw std::runtime_error("Base API URL is empty");

    network = new QNetworkAccessManager(this);
}

QJsonObject Utils::Api::Get(const QString &endpoint, const PropsMap &props, const QString& token)
{
    auto getUrl = PrepareGetUrl(endpoint, props);

    QNetworkRequest request(getUrl);
    if (!token.isEmpty()) {
        QString hdrName = "Authorization";
        auto hdrValue = QString("Bearer %1").arg(token);

        request.setRawHeader(hdrName.toUtf8(), hdrValue.toUtf8());
    }

    QNetworkReply* reply = network->get(request);

    WaitForReply(reply);

    auto res = reply->readAll();
    auto resDoc = QJsonDocument::fromJson(res);

    return resDoc.object();
}

QJsonObject Utils::Api::Post(const QString &endpoint, const QJsonObject &data, const QString& token)
{
    auto postUrl = QUrl(QString("%1/%2").arg(baseUrl).arg(endpoint));

    QNetworkRequest request(postUrl);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    if (!token.isEmpty()) {
        QString hdrName = "Authorization";
        auto hdrValue = QString("Bearer %1").arg(token);

        request.setRawHeader(hdrName.toUtf8(), hdrValue.toUtf8());
    }

    QJsonDocument dataDoc;
    dataDoc.setObject(data);

    QNetworkReply* reply = network->post(request, dataDoc.toJson());

    WaitForReply(reply);

    auto res = reply->readAll();
    auto resDoc = QJsonDocument::fromJson(res);

    return resDoc.object();
}

QImage Utils::Api::LoadImage(const QString& imgUrl)
{
    QNetworkRequest request(imgUrl);
    QNetworkReply* reply = network->get(request);

    WaitForReply(reply);

    auto res = reply->readAll();
    auto img = QImage::fromData(res);

    return img;
}

QUrl Utils::Api::PrepareGetUrl(const QString &endpoint, const PropsMap &props) const
{
    auto urlStr = QString("%1/%2").arg(baseUrl).arg(endpoint);

    if (!props.empty()) {
        bool first = true;

        for (const auto& p: props)
        {
            auto fixValue = QString::fromUtf8(QUrl::toPercentEncoding(p.value));

            auto propStr = QString(first ? "?%1=%2" : "&%1=%2")
                .arg(p.name)
                .arg(fixValue);
        }
    }

    return urlStr;
}

void Utils::Api::WaitForReply(QNetworkReply *reply)
{
    if (!reply)
        return;

    QEventLoop evtLoop;
    connect(reply, SIGNAL(finished()), &evtLoop, SLOT(quit()));

    evtLoop.exec();
}

#include "MainWindow.hpp"

#include <QMessageBox>

#include "ProjEditDlg.hpp"

UI::MainWindow::MainWindow(Data::ProjList& pl, Data::QuwiAPI& api, QWidget *parent):
    QWidget(parent),

    mainBox(nullptr),

    topPanel(nullptr),
    projListView(nullptr),

    projList(pl),
    quwiApi(api)
{
    InitUI();
}

UI::MainWindow::~MainWindow()
{
}

void UI::MainWindow::InitUI()
{
    setMinimumSize(600, 400);

    mainBox = new QVBoxLayout();
    mainBox->setContentsMargins(0, 0, 0, 0);
    mainBox->setSpacing(0);
    setLayout(mainBox);

    topPanel = new TopPanel();
    mainBox->addWidget(topPanel);
    connect(topPanel, SIGNAL(logout()), this, SLOT(OnLogout()));

    projListView = new ProjListView();
    projListView->SetProjectList(&projList);
    mainBox->addWidget(projListView, 1);
    connect(projListView, &ProjListView::editProject, this, &MainWindow::OnEditProject);
}

void UI::MainWindow::OnLogout()
{
    QMessageBox::information(this, "Logout", "Logout was requested");
}

void UI::MainWindow::OnEditProject(int index)
{
    Data::Project& proj = projList[index];

    ProjEditDlg editDlg(proj);
    if (editDlg.exec() == QDialog::Accepted)
    {
        try
        {
            auto newProjName = editDlg.GetProjectName();
            if (newProjName == proj.name)
                return;

            quwiApi.SetProjectName(proj.id, newProjName);

            proj.name = newProjName;
            projListView->SetProjectList(&projList);
        }
        catch (...)
        {
            QMessageBox::critical(this, "Edit project", "Unable to change project name");
        }
    }
}

#pragma once

#include <QListView>

#include "../../../Data/Project.hpp"

namespace UI
{

class ProjListView:
    public QListView
{
    Q_OBJECT

public:
    explicit ProjListView(QWidget* parent = nullptr);

    void SetProjectList(const Data::ProjList* projList);

signals:
    void editProject(int index);

protected slots:
    void OnItemDoubleClicked(const QModelIndex &index);
};

}

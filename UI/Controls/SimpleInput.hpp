#pragma once

#include <QLineEdit>

namespace UI
{

class SimpleInput:
    public QLineEdit
{
    Q_OBJECT

public:
    SimpleInput(QWidget *parent = nullptr);
};

}

#include "ProjEditDlg.hpp"

UI::ProjEditDlg::ProjEditDlg(const Data::Project& initData, QWidget *parent):
    QDialog(parent),

    mainBox(nullptr),

    nameLbl(nullptr),
    nameEdt(nullptr),

    okBtn(nullptr)
{
    InitUI(initData);
}

QString UI::ProjEditDlg::GetProjectName() const
{
    return nameEdt->text();
}

void UI::ProjEditDlg::InitUI(const Data::Project& initData)
{
    setWindowTitle("Edit project");
    setContentsMargins(25, 10, 25, 15);

    mainBox = new QHBoxLayout();
    mainBox->setSizeConstraint( QLayout::SetFixedSize );
    setLayout(mainBox);

    nameLbl = new QLabel("Name: ");
    mainBox->addWidget(nameLbl);

    nameEdt = new SimpleInput();
    nameEdt->setText(initData.name);
    mainBox->addWidget(nameEdt, 1);

    okBtn = new SimpleBtn("OK");
    mainBox->addWidget(okBtn);
    connect(okBtn, &SimpleBtn::clicked, this, &ProjEditDlg::OnOk);
}

void UI::ProjEditDlg::OnOk()
{
    accept();
}

#pragma once

#include <QString>
#include <QJsonObject>

namespace Utils
{

QString ObjToStr(const QJsonObject& obj);

}

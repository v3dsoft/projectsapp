#include "ProjListView.hpp"

#include "ProjListModel.hpp"
#include "ProjListDelegate.hpp"

namespace
{

const QString projLVStyle = R"style(
    QListView {
        background: #f4f4f4;
        border: none;
    }
)style";

}

UI::ProjListView::ProjListView(QWidget *parent):
    QListView(parent)
{
    setStyleSheet(projLVStyle);

    auto* del = new ProjListDelegate();
    setItemDelegate(del);

    connect(this, &ProjListView::doubleClicked, this, &ProjListView::OnItemDoubleClicked);
}

void UI::ProjListView::SetProjectList(const Data::ProjList *projList)
{
    if (projList)
    {
        auto* projListMdl = new ProjListModel(*projList);
        setModel(projListMdl);
    }
    else
        setModel(nullptr);
}

void UI::ProjListView::OnItemDoubleClicked(const QModelIndex &index)
{
    emit editProject(index.row());
}

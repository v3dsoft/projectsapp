#pragma once

#include <QRect>

namespace Utils
{

QRect FitImgRect(const QRect& boundRect, const QSize& imgSize);

}
